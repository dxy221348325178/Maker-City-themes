<!-- 特色项目 -->
<section>
	<div class="view_all__container__YrE2y">
		<div class="view_all__header__1nFpJ">
			<p class="view_all__title__3D87k">
			  	<font style="vertical-align: inherit;">特色项目</font>
			</p>
			<a class="base__lightTextLink__36ptW base__link__1t_o9 typography__linkS__2giV8 typography__bodyS__1HnD8" href="<?php echo home_url()?>/index-project-more">
			  	<font style="vertical-align: inherit;">查看全部</font>
			</a>
		</div>
		<div class="horizontal_scroll__wrapper__2RoY- undefined">
			<div class="horizontal_scroll__scrollContainer__1iBHq hckui__layout__noScrollBar">
				<div class="horizontal_scroll__itemsContainer__2IETC undefined">
				<!-- 首页项目展示信息显示 -->
				<?php echo do_shortcode("[recent-project]");?>
				</div>
			</div>
		</div>
	</div>
</section>