<?php
/**
 * Template Name: 全部平台
 */
get_header();
?>
<!-- 平台框架 -->
<section>
  <div id="main">
    <div id="content">
      <div class="container">
        <h1 class="default-h1">平台与系统</h1>
        <div class="row equal-columns">
          <?php the_post(); the_content();
          ?>
        </div>
      </div>
    </div>
  </div> 
</section>
<?php get_footer();?>
