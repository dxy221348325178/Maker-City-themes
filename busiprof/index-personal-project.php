<div class="profile_form__root__W_3i4 dashboard__root__AYT2h">
    <div class="dashboard__container__3tlv6">
  		<header class="page_header__pageHeaderContainer__1bHey page_header__header__1aguu">    
            <a href="<?php echo home_url()?>/create-pro/" class="buttons__genericLight__35nOw buttons__generic__4Bla8 buttons__button__lYBnk">创建一个新的项目</a>
        </header>  
   		<section class="dashboard__section__2K8l2">
	    	<div class="dashboard__projectList__3YuBy">
	     		<div class="grid__hScrollSm__KIJTv grid__gridBasic3Max__1vlEi grid__gridBasic__fjt5B grid__grid__1QeD6 grid__guttersH__2MYvz grid__guttersV__3M28R">
					<?php $uid = query_this_url_user_id(); do_shortcode("[show-user-projects uid={$uid}]"); ?>
	     		</div>
	    	</div>
   		</section>
  	</div>
</div>
