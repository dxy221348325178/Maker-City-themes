<?php 
/* 	
* Template Name: 规则模板
*/
get_header();
the_post(); the_content();
?>
<section class="main-section">
    <div class="container" >
        <div class="row">
            <div class="col-md-8">
                <section class="section-container" id="rules">
            	    <center>
            		    <h2 >
            		        <div class="title">
                            <!-- 字体垂直 -->
                                <font style="vertical-align: inherit;">比赛规则 </font>
            		        </div>
            		    </h2>
            	    </center>
                    <div class="section-content medium-editor">
                        <h3>
                            <font style="vertical-align: inherit;">要求</font>
                        </h3>
                        <p>
                            <span>
                                <font style="vertical-align: inherit;">
                                    要获得评审资格，参赛作品必须符合以下内容/技术要求：
                                </font>
                            </span>
                            <strong>
                                <font style="vertical-align: inherit;">
                                比赛日期（2018年4月12日，太平洋标准时间下午8点至2018年7月12日，太平洋标准时间下午11:59）
                                </font>
                            </strong>
                        </p>
                        <ul>
                            <li>
                                <font style="vertical-align: inherit;">
                                    您必须创建一个免费的Hackster.io帐户并注册成为本次比赛的参与者。
                                </font>
                            </li>
                        </ul>
                        <ul>
                            <li>
                                <font style="vertical-align: inherit;">所有项目必须以英文提交。</font>
                            </li>
                        </ul>
                        <ul>
                            <li>
                                <span>
                                    <font style="vertical-align: inherit;">
                                        您的项目必须用于连接到
                                    </font>
                                </span>
                                <strong>
                                    <font style="vertical-align: inherit;">
                                        Maxim Integrated MAX32620FTHR的器件
                                    </font>
                                </strong>
                            </li>
                        </ul>
                        <ul>
                            <li>
                                <font style="vertical-align: inherit;">
                                    根据“项目”模板，您的项目必须完整记录在黑客手中。
                                </font>
                            </li>
                        </ul>
                        <ul>
                            <li>
                                <font style="vertical-align: inherit;">
                                    您的最终项目必须在项目模板中提交。
                                </font>
                            </li>
                        </ul>
                        <ul>
                            <li>
                                <font style="vertical-align: inherit;"> 
                                    您的最终项目必须包含以下材料：
                                </font>
                            </li>
                        </ul>
                        <ul>
                            <li>
                                <font style="vertical-align: inherit;">图片</font>
                            </li>
                        </ul>
                        <ul>
                            <li>
                                <font style="vertical-align: inherit;">说明</font>
                            </li>
                        </ul>
                        <ul>
                            <li>
                                <font style="vertical-align: inherit;">原理图</font>
                            </li>
                        </ul>
                        <ul>
                            <li>
                                <font style="vertical-align: inherit;">码</font>
                            </li>
                        </ul>
                        <ul>
                            <li>
                                <font style="vertical-align: inherit;">CAD [如果有的话]</font>
                            </li>
                        </ul>
                        <p>
                            <font style="vertical-align: inherit;">此外：</font>
                        </p>
                        <ul>
                            <li>
                                <font style="vertical-align: inherit;">
                                    您的参赛作品必须是您自己的原创作品; 
                                </font>
                                <font style="vertical-align: inherit;">和</font>
                            </li>
                        </ul>
                        <ul>
                            <li>
                                <font style="vertical-align: inherit;">
                                    您的参赛作品不能被选为任何其他比赛的获胜者; 
                                </font>
                                <font style="vertical-align: inherit;">和</font>
                            </li>
                        </ul>
                        <ul>
                            <li>
                                <font style="vertical-align: inherit;">
                                    您必须获得您提交参赛作品所需的任何和所有同意，批准或许可; 
                                </font>
                                <font style="vertical-align: inherit;">和</font>
                            </li>
                        </ul>
                        <ul>
                            <li>
                                <font style="vertical-align: inherit;">
                                    除非您已获得使用这些材料的许可，否则您的作品可能不包含任何第三方商标（徽标，名称）或受版权保护的材料（音乐，图像，视频，可识别的人）。
                                </font>
                            </li>
                        </ul>
                    </div>
                </section>
            </div>
        </div>
    </div>
</section>
<?php get_footer();  ?> 
