<?php 
/*  
* Template Name: Home
*/
?>
<?php get_header();?>
<div id="outer-wrapper">
  <div id="main">
    <div data-reactroot="">
    <!-- 主页面轮播图 -->
        <?php echo do_shortcode('[metaslider id="4"]'); ?>
    <!-- 比赛章程 -->
        <?php get_template_part('index', 'contest-regulations') ; ?>
    <!-- 比赛内容显示 -->
        <?php get_template_part('index', 'contents') ; ?>
    </div>
  </div>
</div>
<?php get_footer();  ?> 