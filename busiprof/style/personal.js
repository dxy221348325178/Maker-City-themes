//个人信息表单验证
//验证手机号 
function checkPhone(){ 
  var userphone = document.getElementById('user_phone'); 
  var phonrErr = document.getElementById('phoneErr'); 
  var pattern = /^1[34578]\d{9}$/; //验证手机号正则表达式 
    if(!pattern.test(userphone.value)){ 
        phonrErr.innerHTML="手机号码不合规范";
        phonrErr.className="error";
        return false; 
    }else{ 
        phonrErr.innerHTML="";
        phonrErr.className="success"; 
        return true; 
        }
     

}
//验证身份证号
function checkID(){ 
      var userID = document.getElementById('personal_id'); 
      var IDErr = document.getElementById('IDErr'); 
      var pattern = /^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{4}$/; //验证身份证号正则表达式 
      if(!pattern.test(userID.value)){ 
            IDErr.innerHTML="身份证号码不合法";
            IDErr.className="error";
            return false; 
        }else{ 
            IDErr.innerHTML="";
            IDErr.className="success"; 
            return true; 
        }  
}
// 个人中心表单提交
function form_btn(url){   
    // alert(url);
    // $('#formid').serialize();
    // alert($('#formid').serialize());
    var params1 = $('#formid').serialize();
    // var user_pic = $("#Div1>input[type=file]");
    var prevLink = document.referrer;  
    //表单验证失败阻止提交
    if(checkID() && checkPhone()){
        $.ajax({
            type: 'post',   
            url: url,
            data: params1,
            success:function(data){
                alert("修改成功");
                // alert(prevLink);
                // 判断document.referrer是否为空，
                // 若为空本页面就不是从其它页面跳转过来的，就将页面跳转至网站首页
                if($.trim(prevLink)==''){  
                    location.href = 'www.example.com/index.html';  
                }else{  
                    if(prevLink.indexOf('www.example.com')==-1){    //来自其它站点  
                        location.href = 'www.example.com/index.html';  
                    }  
                    if(prevLink.indexOf('register.html')!=-1){      //来自注册页面  
                        location.href = 'www.example.com/index.html';  
                    }  
                    location.href = prevLink;  
                }  
            },
            error:function(data){
                // alert("提交失败");
            }
        })    
    } else{
        alert("录入失败");
        return false;

    }
    }
//个人信息取消编辑
function form_reset(){
    // alert('1');
    $('#formid')[0].reset();
}

// 点击图片进入选择文件
function btn_file(){
    $("#upload").click(); 
    $("#upload").on("change",function(){
            var objUrl = getObjectURL(this.files[0]) ; //获取图片的路径，该路径不是图片在本地的路径
            // alert(objUrl);
            if (objUrl) {
                $("#headPic").attr("src", objUrl) ; //将图片路径存入src中，显示出图片
            }
        });
}

//图片上传
function submit_btn(url){

        // var imgurl = document.getElementById("upload").value;
        var user_pic = $('#upload').val();
        $.ajax({
            url:url,
            type:'post',
            data:{'user_pic':user_pic},
                // fileElementId: "upload", //文件上传域的ID，这里是input的ID，而不是img的
                // dataType: 'json', //返回值类型 一般设置为json
                // contentType: "application/x-www-form-urlencoded; charset=utf-8",
            success: function (data) {
                // alert(data.code+" "+ data.msg);
                alert(data);
                // if (data.code==200){
                //     alert("message");
                //     $("#headPic").attr("src","/market/images/image.png");
                //     //将图片换成默认的+图片
                // }     
            }
        });
    }


//建立一个可存取到该file的url
function getObjectURL(file) {
    var url = null ;
    if (window.createObjectURL!=undefined) { // basic
        url = window.createObjectURL(file) ;
    } else if (window.URL!=undefined) { // mozilla(firefox)
        url = window.URL.createObjectURL(file) ;
    } else if (window.webkitURL!=undefined) { // webkit or chrome
        url = window.webkitURL.createObjectURL(file) ;
    }
    return url ;
}




