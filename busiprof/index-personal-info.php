 <?php 
/*  
* Template Name: 个人中心(轮廓)
*/
	get_header();
	$user = sel_user(query_this_url_user_id());
	get_template_part("index","personal-header");
?>
<div name='content_tag' style="display:block" id='content1'>
<?php 
	show_user_info($user);
 ?>
</div>
<div name='content_tag' style="display:none" id='content2'>
<?php 
	get_template_part("index","personal-project");
 ?>
</div>
<div name='content_tag' style="display:none" id='content3'>
<?php 
	get_template_part("index","personal-activity");
 ?>
</div>	
<script>
    $("#tab>li").click(function(){	
        var tag_id = 'content'+$(this).attr('value');
        var tag = "#"+tag_id;
    	$("#tab>li").css('color','#C1C1C1');
    	$(this).css('color','#3367D6');
        $("div[name='content_tag']").css('display','none');
        $(tag).css('display','block');
    });
</script>
<?php  get_footer();?>
