<?php
 require( dirname(dirname(dirname(dirname( __FILE__ )))) . '/wp-load.php' );
/****************************************************************
*当前用户加入当前比赛
/***************************************************************/
function user_join_contest(){
    global $wpdb;
    require( dirname( __FILE__ ) . '/join-contest.php');
    // return "select ID from {$wpdb->prefix}posts where post_title = '{$title}' ";
}
/****************************************************************
*输出项目详情
/***************************************************************/
function show_project_particular(){
    global $wpdb;
    global $current_user;
    wp_get_current_user();
    //取项目id
    $pro_id = quert_projectid_by_url();
    if(!this_post_is_project($pro_id)){
        get_header();
        die("<br><br><br><br><center><h2>您访问的项目不存在{$pro_id}</h2></center>");
    }
    $pro = get_post($pro_id,ARRAY_A);
    $res = $wpdb->get_results("select meta_key , meta_value from {$wpdb->prefix}postmeta where post_id = {$pro_id}",ARRAY_A);
    foreach($res as $k=>$v){
        if($v['meta_key'] == 'pro_users') $pro_meta['pro_users'][] = $v['meta_value'];
        else if($v['meta_key'] == 'pro_thing_h') $pro_meta['pro_thing_h'][] = $v['meta_value'];
        else if($v['meta_key'] == 'pro_thing_s') $pro_meta['pro_thing_s'][] = $v['meta_value'];
        else $pro_meta[$v['meta_key']] = $v['meta_value'];
    }
    if($current_user->ID == $pro['post_author']){
        require(dirname( __FILE__ )."/page-extend/pro-edit-info.php");  
    }
    //项目详情
    require(dirname( __FILE__ )."/page-extend/proinfo.php");
}
?>
<?php 
    //显示项目 (特色项目下的 比赛下的 平台下的 用户下的)
    function show_project($projects){
        // echo '<pre>';
        // var_dump($projects);
        // echo '</pre>';
        // 判断是否在后台取到项目
        if(!$projects) return null;
        foreach($projects as $key => $value){
            $author = sel_user($value['post_author']);
            any_project($value,$author);
        }
    }   
    //显示项目样式 $author形参默认为空
    function any_project($res_pro,$author= ''){
    ?>
        <div name="one_project" style="margin-top:20px;" class="mobile-scroll-row-item project-thumb-container col-sm-6 col-md-4 col-lg-3 has-data">
            <div class="project_card__wrapper__1PrMs cards__wrapper__ybxCu">
                <div class="project_card__cardBorder__3AB3C project_card__card__3ZBbS cards__card__nIkNU cards__cardWithBorder__X_vK9 cards__card__nIkNU">
                    <a name = "project_url"   class="project_card__imageContainer__1cw7g" target="_blank" href="<?php echo output_project_url($res_pro['ID']);?>">
                        <div name='project_pic' class="project_card__itemImage">
                            <div class="project_card__lazyImage lazy_image__root">
                              <img alt="" style="width:257px;height:193px;"  class="lazy_image__image lazy_image__fadeIn" src="<?php echo home_url()."/wp-content/".$res_pro['pro_pic']?>" />
                            </div>
                        </div>
                        <div class="project_card__overlay__2W-4e">
                            <span class="typography__bodyS__1HnD8">
                                <font class="font_pro_title_type"><?php echo $res_pro['post_excerpt']?>
                                </font>
                            </span>
                        </div>
                     </a>
                    <div class="project_card__body__S0LrW cards__body__3yUOQ">
                        <a name='project_title' class="project_card__title__3pebx cards__title__1kNED base__darkTextLink__1jxXU base__link__1t_o9 typography__linkM__qx1dO typography__bodyM__fAsI4" target="_blank" href="<?php echo output_project_url($res_pro['ID']);?>"
                            <font class="font_pro_title_type">
                                <?php echo $res_pro['post_title']?>
                            </font>
                        </a>
                        <div>
                            <a class="project_card__authorLink__1yXKr project_card__authorHeader__1e0xR typography__bodyS__1HnD8 base__textColorLink__izyDW base__link__1t_o9" href="<?php echo home_url().'/user/'.$user['uname'].'/project';?>">
                                <font class="font_pro_title_type">
                                    <?php echo $author['nickname'];
                                    ?>
                                </font>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php }?>
<?php
    // 显示特色（首页）项目
    function unique_project_function(){
        global $wpdb;
        $res_characteristic_project = query_characteristic_project(4);
        // 获取表里面的关于项目的信息
        if(count($res_characteristic_project)>0){
            foreach ($res_characteristic_project as $key => $value) { 

                $post = get_post($value['post_id'],ARRAY_A);
                $post_meta = get_post_meta($value['post_id']);
                $author = get_user_meta($post['post_author'],"nickname",true);

            ?>
                <div class="view_all__scrollerItemProject__20RRh view_all__scrollerItem__IfvA6">
                    <div class="project_card__cardBorder__3AB3C project_card__card__3ZBbS cards__card__nIkNU cards__cardWithBorder__X_vK9 cards__card__nIkNU">
                        <a class="project_card__imageContainer__1cw7g" target="_blank" href="<?php echo output_project_url($post['ID']);?>">
                            <div class="project_card__itemImage__2W1r8">
                                <div class="project_card__lazyImage__18nwi lazy_image__root__3J0Tw">
                                    <img alt="" class="lazy_image__image__hVuzg lazy_image__fadeIn__1kICV" src="<?php echo home_url()."/wp-content/".$post_meta['pro_pic']['0']?>" />
                                </div>
                            </div>
                            <div class="project_card__overlay__2W-4e">
                                <span class="typography__bodyS__1HnD8">
                                    <font class="font_pro_title_type">
                                        <?php echo $post['post_excerpt']?>
                                    </font>
                                </span>
                            </div>
                        </a>
                        <div class="project_card__body__S0LrW cards__body__3yUOQ">
                            <a class="project_card__title__3pebx cards__title__1kNED base__darkTextLink__1jxXU base__link__1t_o9 typography__linkM__qx1dO typography__bodyM__fAsI4" href="<?php echo output_project_url($post['ID']);?>">
                                <font class="font_pro_title_type">
                                    <?php echo $post['post_title']?>
                                </font>
                            </a>
                            <div>
                                <a class="project_card__authorLink__1yXKr project_card__authorHeader__1e0xR typography__bodyS__1HnD8 base__textColorLink__izyDW base__link__1t_o9" href="<?php echo output_project_url($post['ID']);?>">
                                    <font class="font_pro_title_type">
                                        <?php echo $author;?>
                                    </font>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
<?php       }
        }
    }
    //没有项目时显示空项目框
    function no_project(){
        ?>
        <div class="view_all__scrollerItemProject__20RRh view_all__scrollerItem__IfvA6">
            <div class="project_card__cardBorder__3AB3C project_card__card__3ZBbS cards__card__nIkNU cards__cardWithBorder__X_vK9 cards__card__nIkNU">
                <a class="project_card__imageContainer__1cw7g" target="_blank" href="">
                    <div class="project_card__itemImage__2W1r8">
                        <div class="project_card__lazyImage__18nwi lazy_image__root__3J0Tw">
                            <img alt="" class="lazy_image__image__hVuzg lazy_image__fadeIn__1kICV" src="" />
                        </div>
                    </div>
                    <div class="project_card__overlay__2W-4e">
                        <span class="typography__bodyS__1HnD8">
                            <font class="font_pro_title_type">
                                
                            </font>
                        </span>
                    </div>
                </a>
                <div class="project_card__body__S0LrW cards__body__3yUOQ">
                    <a class="project_card__title__3pebx cards__title__1kNED base__darkTextLink__1jxXU base__link__1t_o9 typography__linkM__qx1dO typography__bodyM__fAsI4" href="">
                        <font class="font_pro_title_type">
                           
                        </font>
                    </a>
                    <div>
                        <a class="project_card__authorLink__1yXKr project_card__authorHeader__1e0xR typography__bodyS__1HnD8 base__textColorLink__izyDW base__link__1t_o9" href="">
                            <font class="font_pro_title_type">
                               
                            </font>
                        </a>
                    </div>
                </div>
            </div>
        </div> 
<?php }
//注册短代码
function register_shortcodes(){
    add_shortcode('recent-project', 'unique_project_function');
}
add_action( 'init', 'register_shortcodes');
    
?>
<?php
/**
 * 大赛参与者函数
 */
    // 大赛参与者展示
    function show_participants($user){
        
        if(!$user) return null;
        foreach($user as $key => $partner) {
            one_participant($partner);
            }
    }
    //参与者样式
    function one_participant($partner){

        $pro_num = sel_user_pro_num($partner['id']);
        // echo "<pre>";
        // var_dump($pro_num);
        // echo "</pre>";
    if($partner['pic'])
         $user_pic = $partner['pic'];
    else
        $user_pic = home_url()."/wp-content/uploads/ultimatemember/default/default.png";
?> 
         <div class="col-sm-6 col-md-4 col-lg-3">
            <div class="user-card media">
                <div class="media-left">
                    <a href="/harshbamoriacool">
                        <img class="img-circle media-object" srcset="" alt="lalala" src="<?php echo $user_pic?>" />
                    </a>
                </div>
                <div class="media-body">
                    <h3 class="hckui__typography__h3">
                        <a class="hckui__typography__link" href="/harshbamoriacool">
                            <font class="font_pro_title_type">
                            <?php echo $res['0']['display_name']?>
                            </font>
                        </a> 
                    </h3>
                    <div class="hckui__typography__bodyS hckui__typography__pebble">
                        <font class="font_pro_title_type">
                            <?php echo $pro_num;?>个项目
                        </font>
                    </div>
                    <div class="hckui__layout__marginTop15">
                        <span style="display:inline-block;vertical-align:bottom;margin-right:10px">
                            <span >
                                <button class="hckui__buttons__sm" type="button" >
                                    <font class="font_pro_title_type">跟随</font>
                                </button>
                            </span> 
                        </span>
                        <a class="hckui__buttons__sm hckui__buttons__secondary reactPortal"  href="">
                            <font class="font_pro_title_type">联系</font>
                        </a>
                    </div>
                </div>
            </div>
        </div>
<?php }?>
<?php
/****************************************************************
*显示所有比赛框函数
/***************************************************************/
function show_all_contests(){   
    $posts = get_children(get_the_ID(),ARRAY_A);
    if($posts){
        foreach($posts as $k => $v){
            $value = get_post_meta($v['ID']);
?>      
            <div class="challenge-thumb-full">
                <div class="row">
                    <div class="col-md-3 col-sm-4 details-container">
                        <div class="details">
                            <h4>
                                <font class="font_pro_title_type">
                                   <a href="<?php echo get_permalink($v['ID']).get_post_meta($v['ID'],'contest_url',true); ?>"><?php echo $v['post_title'] ?>
                                   </a>
                                </font>
                            </h4>
                            <div class="small challenge-sponsors">
                                <strong>
                                    <a href="javascript:void(0)">
                                        <font class="font_pro_title_type">
                                            <?php echo $value['contest_brief']['0'] ?>
                                        </font>
                                    </a>
                                </strong>
                            </div>
                            <div class="bottom">
                                <div class="date">
                                    <font class="font_pro_title_type">报名时间 </font>
                                    <br />
                                    <strong>
                                        <font class="font_pro_title_type">
                                            <time datetime="">
                                            <?php echo get_post_meta($v['ID'],'end_time',true); ?>
                                            </time>
                                        </font>
                                    </strong>
                                </div>
                                <a class="btn btn-primary" href="<?php echo get_permalink($v['ID']).get_post_meta($v['ID'],'contest_url',true); ?>">参加比赛</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9 col-sm-8 hidden-xs">
                        <div class="row">
                            <!-- 比赛展示图片 -->
                            <div class="col-md-9 col-sm-8" >
                                <?php 
                                    $contest_bg_id = get_post_meta($v['ID'],'contest_background',true);
                                    $contest_bg = get_the_guid($contest_bg_id,'guid',true);
                                 ?>
                                <div class="cover img-loader loaded" id="background-image-contest-brief" style="background-image: url(<?php echo $contest_bg; ?>?auto=compress%2Cformat&w=600&h=300&fit=min);"></div>
                            </div>
                            <!-- 奖品 -->
                            <div class="col-md-3 col-sm-4">
                                <div class="prize">
                                    <div>
                                        <?php 
                                            $prize = [];
                                            for($i = 0;$i<10;$i++){ 
                                        ?>
                                        <?php 
                                            $prize_word = get_post_meta($v['ID'],'prize_word_'.$i,true);
                                            if(!$prize_word) continue;
                                            $prize[$i]['word'] = $prize_word;

                                            $prize_pic_id = get_post_meta($v['ID'],'prize_pic_'.$i,true);

                                            if($prize_pic_id){
                                                $prize_pic = get_the_guid(
                                                    $prize_pic_id,'guid',true);
                                            }else{
                                                $prize_pic = '';
                                            }
                                            $prize[$i]['pic'] = $prize_pic;
                                        ?>
                                        <?php 
                                            }
                                            // echo '<pre>';
                                            // var_dump($prize);
                                            //  echo '</pre>'; 
                                        ?>
                                        <h5>
                                            <i class="fa fa-star"></i>
                                            <span>最高奖励</span>
                                        </h5>
                                    </div>
                                    <?php 
                                        if(count($prize)>0){
                                            foreach($prize as $k=>$v){
                                    ?>
                                    <img alt="Top prize" class="img-loader loaded" src="<?php echo $v['pic'] ?>">
                                    <div class="description">
                                        <?php echo $v['word']; ?>
                                    </div>
                                    <?php
                                            break; 
                                            }
                                        }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<?php   
        }
    }
} 
//注册短代码-->显示所有比赛框
add_shortcode( 'show-all-contests', 'show_all_contests' );
?>
<?php 
    //显示比赛背景图片
    function show_big_pic($contest){ 
    if(!$contest) return null;
?>
    <div id="main">
        <div id="challenge">
            <section class="top-banner">
                <div class="top-banner-image hidden-xs user-bg" style="background-image:url('<?php echo get_the_guid($contest['contest_background'],true);?>');opacity:1;z-index:0">
                    <div class="top-banner-image-inner" >
                        <div class="container">
                            <div class="media" >
                                <h1><?php echo $contest['post_title']; ?></h1>
                                <p><?php echo $contest['contest_brief']; ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php } ?>
<?php 
    //主页显示平台函数
    function unique_platform_function($platform){
         //平台logo
        $platforms_pro_pic = get_the_guid( $platform['platforms_pic']);
        //平台下项目数量
        $platforms_pro_num = query_platform_project_num($platform['ID']);

?>
            <div class="view_all__scrollerItemProject__20RRh view_all__scrollerItem__IfvA6">
               <div class="cards__wrapper__ybxCu">
                  <div class="cards__cardWithBorder__X_vK9 cards__card__nIkNU cards__cardBody__2Se_o">
                     <div class="cards__icon__2vk0z">
                        <a href="<?php echo home_url().'/platforms/'.$platform['post_name'].'/project' ?>">
                           <div class="cards__lazyImage__2l9WG lazy_image__root__3J0Tw" style="height: 75px; width: 75px;">
                             <img alt="" class="lazy_image__image__hVuzg lazy_image__fadeIn__1kICV" src="<?php echo $platforms_pro_pic?>" />
                           </div>
                        </a>
                     </div>
                     <a class="cards__title__1kNED base__darkTextLink__1jxXU base__link__1t_o9 typography__linkM__qx1dO typography__bodyM__fAsI4" href="">
                        <font style="vertical-align: inherit;">
                            <?php echo $platform['post_title'];?>
                        </font>
                     </a>
                     <section class="typography__bodyS__1HnD8">
                        <font style="vertical-align: inherit;">   
                           <?php echo $platform['platforms_info'];?>
                        </font>
                     </section>
                     <div class="cards__stats__29EsN typography__bodyXS__3E4Ur">
                        <span class="cards__stat__1EnZj">
                           <font style="vertical-align: inherit;">
                           <!-- 平台下面的用户 -->
                           </font>
                        </span>
                        <span class="cards__stat__1EnZj">
                           <font style="vertical-align: inherit;">
                             <?php echo $platforms_pro_num?>个项目
                           </font>
                        </span>
                     </div>
                     <center>
                        <a href="<?php echo home_url().'/platforms/'.$platform['post_name'].'/project' ?>">
                           <button class="buttons__button__lYBnk buttons__sm__2DQ74 buttons__base__3w_1G buttons__dark__wQHEh" type="button">
                              <font style="vertical-align: inherit;">查看项目</font>
                           </button>
                        </a>
                     </center>
                  </div>
               </div>
            </div>
<?php }

    //显示平台
    function show_the_platform($platform){
        //平台logo
        $platforms_pro_pic = get_the_guid( $platform['platforms_pic']);
        //平台下项目数量
        $platforms_pro_num = query_platform_project_num($platform['ID']);
?>  
    <!-- 引入单个平台 -->
    <div class="col-sm-6 col-md-4 col-lg-3 mobile-scroll-row-item" >
        <div class="channel-thumb">
            <div class="channel-thumb-image">
                <a href="<?php echo home_url().'/platforms/'.$platform['post_name'].'/project' ?>">
                     <img alt="" class="img-loader loaded" src="<?php echo $platforms_pro_pic;?>" />
                </a>
            </div>
            <h4>
                <a href="<?php echo home_url().'/platforms/'.$platform['post_name'].'/project' ?>">
                   <font style="vertical-align: inherit;">
                      <?php echo $platform['post_title'];?>
                   </font>
                </a>
            </h4>
            <p>
                <font style="vertical-align: inherit;">
                   <?php echo $platform['platforms_info']?>
                </font>
            </p>
            <div class="spacer"></div>
                <div class="stats">
                    <span>
                        <font style="vertical-align: inherit;">
                              <?php echo $platforms_pro_num?>个项目
                        </font>
                    </span>
                    <span>
                        <font style="vertical-align: inherit;"><!-- 平台成员 --></font>
                    </span>
                </div>
                <!--
                <div class="label label-primary" style="margin-top:5px;display:block">-->
                    <a href="<?php echo home_url().'/platforms/'.$platform['post_name'].'/project' ?>" class="btn btn-primary">
                              查看项目
                    </a>
                    <!--
                </div>-->
            </div>
        </div>
<?php }
    //平台顶部导航
    function show_platform_header($platform){
        // var_dump($platform); 
?>  
        <div class="header__header__VBJ_k">
            <div class="header__contentWrapper__1Akwc">
               <div class="header__avatar__2DDH2">
                  <img src="<?php echo get_the_guid($platform['platforms_pic'])?>" alt=""/>
               </div>
               <div class="header__content__2SKdY">
                  <h1 class="typography__headerM__2b_eo header__name__2Vu9k">
                     <font style="vertical-align: inherit;"><?php echo $platform['post_title'];?></font>
                  </h1>
                  <div class="typography__bodyM__fAsI4 header__body__XmmYv">
                     <font style="vertical-align: inherit;"><?php echo $platform['platforms_info'];?></font>
                  </div>
                  <!--<a class="base__link__1t_o9 typography__bodySBold__2MXW3 typography__bodyS__1HnD8 header__callToAction__2XEVS" href=""  target="_blank">
                     <font style="vertical-align: inherit;">购买平台相关硬件 </font>
                  </a>-->
                  <a class="base__lightTextLink__36ptW base__link__1t_o9 typography__bodySBold__2MXW3 typography__bodyS__1HnD8" href="<?php echo $platform['platforms_url'] ?>"  target="_blank">
                     <font style="vertical-align: inherit;">访问网站</font>
                  </a>
               </div>
            </div>
         </div>
<?php  }

    // 个人中心公共导航部分
    function show_user_header($user){

?>
    <div class="layout__headerContainer__3tDkO layout__container__3L3Cc navbar__container__1RxhJ">
    <div class="layout__wrapperCenter1170__3xUpU layout__wrapperCenter__6qTjc">
        <div class="user_card__root__13tSO">
            <img class="user_card__avatar__1GYq5" src="<?php echo $user['pic'];?>" />
                <div class="userInfo">
                    <h1>
                        <font style="vertical-align: inherit;">个人中心</font>
                    </h1>
                </div>
            </div>
        <div class="navbar__themeLight__1OkVg navbar__center__WcYHj">
            <ul class="nav nav-tabs" id="tab">
                <li class="navbar__listItem__1JH7v"  id="tab1" value="1">
                    <div class="web" >
                            <font style="vertical-align: inherit;">信息</font>
                    </div>
                </li>
                <li class="navbar__listItem__1JH7v"  id="tab2" value="2">
                    <div class="web" >
                            <font style="vertical-align: inherit;">项目</font>
                    </div>
                </li>
                <li class="navbar__listItem__1JH7v"  id="tab3" value="3">
                    <div class="web" >
                        
                            <font style="vertical-align: inherit;">活动</font>
                    </div>
                </li>
            </ul>
           
        </div>
    </div>
</div>
<?php
}
    // 个人中心信息
    function show_user_info($user){
        // var_dump($user);
?>
    <div class="profile_form__root__W_3i4 dashboard__root__AYT2h " name="content1"  >
        <div class="dashboard__container__3tlv6 profile_form__container__YU2wb ">
            <!--<div class="profile_form__rowOne__WR9OX">
                <div class="dropzone ">
                    <div>
                        <input type="file" 
                          id="upload" name="file" style="display:none" />
                        <div id="personal_pic" class="avatar__image__8hIOT">
                            <img onclick="btn_file()" id="headPic"  src="<?php  echo home_url()?>/wp-content/uploads/ultimatemember/default/default.png"  >
                        </div>
                    </div>
                </div>
                <div id ="button_draft" >
                <button  class="profile_form__profileLink__i8b-P" onclick="submit_btn('<?php echo get_template_directory_uri(); ?>/get_personal.php')"   href="#">修改头像</a>
                </button>
                </div>
            </div>-->
            <div class="profile_form__rowTwo__1aDUl">
                <div class="profile_form__formWrapper__no-2J">
                    <form  id="formid">
                        <div>
                            <div id="form-name-0" class="hckui__inputs__formGroup">
                                <label class="profile_form__label__2bfhG inputs__label__2A50W" for="name">用户名</label>
                                    <input type="text" class="hckui__inputs__input " name="name" placeholder="" readonly value="<?php echo $user['uname'];?>" />             
                            </div>
                        </div>
                        <div>
                            <div id="form-user_name-1" class="hckui__inputs__formGroup">
                                <label class="profile_form__label__2bfhG inputs__label__2A50W" for="user_name">昵称</label>
                                <input type="text" class="hckui__inputs__input " name="user_name" placeholder=""  value="<?php echo $user['nickname'];?>" />
                            </div>
                        </div>
                        <div>
                            <div id="form-skills-5" class="hckui__inputs__formGroup">
                                <label class="profile_form__label__2bfhG inputs__label__2A50W" for="skills">邮箱</label>
                                <input type="text" class="hckui__inputs__input " name="user_email" placeholder="" value="<?php echo $user['email'];?>" />
                            </div>
                        </div> 
                        <div>
                            <div id="form-skills-5"  class="hckui__inputs__formGroup">
                                <label class="profile_form__label__2bfhG inputs__label__2A50W" for="skills">电话</label>
                                <input type="text" onBlur="checkPhone()" onInput="checkPhone()" 
                                 class="hckui__inputs__input " name="user_phone" id="user_phone" placeholder="" value="<?php echo $user['tel_num'];?>" />(参赛必填)
                                <?php 
                                 if($user['tel_num']==""){
                                ?>
                                 <span class="default" id="phoneErr">请输入11位手机号码</span> 
                                 <?php }else{ ?>
                                 <span class="default" id="phoneErr"></span>

                                <?php } ?> 
                                 
                            </div>
                        </div>
                        <div>
                            <div id="form-skills-5" class="hckui__inputs__formGroup">
                                <label class="profile_form__label__2bfhG inputs__label__2A50W" for="skills">身份证号</label>
                                <input type="text" onBlur="checkID()" onInput="checkID()" name="personal_id" class="hckui__inputs__input " id="personal_id" placeholder="" value="<?php echo $user['id_card_num'];?>" />(参赛必填)
                                <?php 
                                if($user['id_card_num']==""){ 
                                 ?>         
                                    <span class="default" id="IDErr">请输入正确的二代身份证号</span>
                                <?php  }else{ ?> 
                                    <span class="default" id="IDErr"></span>
                                <?php } ?>     
                            </div>
                        </div>
                        <div>
                            <div id="form-skills-5" class="hckui__inputs__formGroup">
                                <label class="profile_form__label__2bfhG inputs__label__2A50W" for="skills">自我介绍</label>
                                <textarea  rows="3" cols="20" name="introduced" maxlength="200"> <?php echo $user['description'];?></textarea>
                                   
                                
                            </div>
                        </div>
                    <div class="profile_form__actions__xTNhj">
                        <script type="text/javascript">var url = '';</script>
                        <span>
                            <button class="hckui__buttons__md undefined buttons__cancel__ICYBH buttons__text__33gI- buttons__button__lYBnk " type="button" onclick="form_btn('<?php echo get_template_directory_uri(); ?>/get_personal.php')" >保存</button>
                        </span>
                        <span>
                            <button class="hckui__buttons__md undefined buttons__cancel__ICYBH buttons__text__33gI- buttons__button__lYBnk " type="button" onclick="form_reset()" >取消</button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php }?>
<?php 
    function show_contest_menu_items($contest , $bg_url){
?>
        <section class="top-banner">
            <div class="top-banner-image hidden-xs user-bg" style="background-size:100% 400px;background-image:url('<?php echo $bg_url; ?>');">
            </div>
            <section class="group-nav affixable">
                <div class="group-nav-inner hidden-xs">
                    <ul class="nav nav-tabs" id="menu_contest_ul_id">
                        <li role="presentation" class=""><a href="<?php echo home_url().'/contest/'.$contest.'/brief'; ?>">报名</a></li>
                        <li role="presentation" class=""><a href="<?php echo home_url().'/contest/'.$contest.'/project'; ?>">项目</a></li>
                        <li role="presentation" class=""><a href="<?php echo home_url().'/contest/'.$contest.'/member'; ?>">参与者</a></li>
                    </ul>
                </div>
            </section>
        </section>
<?php
}
		function show_detail_project($pro_meta){
 ?>
		<section class="section-container" id="schematics">
            <a name="#schematics"></a>
            <span class="section-title two-title">原理图</span><hr>
			<div class="section-content">
				<div class="repository">
					<div class="button-content-container">
						<div class="button-content one-font">
							<?php
							  if(isset($pro_meta['diagrams_file'])){
							?>
								项目原理图<hr>
							<?php }else{ ?>
								这个团队没有分享他们的项目原理图
							<?php } ?>
							<div class="buttons" style="display:none">
								<a class="btn btn-primary btn-sm" href="<?php echo home_url().'/wp-content/'?>">下载 </a>
							</div>
						</div>
					</div>
					<?php
					  if(isset($pro_meta['diagrams_file'])){
					?>
					<div class="embed original">
						<img src="<?php echo home_url().'/wp-content/'.$pro_meta['diagrams_file']?>" alt="">
					</div>
					<?php } ?>
				</div>
			</div>
        </section>
        <section class="section-container" id="cad">
            <a name="#cad"></a>
            <span class="section-title two-title">CAD图</span><hr>
            <div class="section-content">
				<div class="repository">
					<div class="button-content-container">
						<div class="button-content one-font">
							<?php
							  if(isset($pro_meta['cad_file'])){
								?>
								项目CAD图<hr>
								<?php }else{ ?>
								这个团队没有分享他们的项目CAD图
							<?php } ?>
							<div class="buttons" style="display:none">
								<a class="btn btn-primary btn-sm" href="<?php bloginfo('url');echo '/wp-content/'.$v['path']?>">下载 </a>
							</div>
						</div>
                    </div>
					<?php
					  if(isset($pro_meta['cad_file'])){
					?>
					<div class="embed original">
						<img src="<?php echo home_url().'/wp-content/'.$pro_meta['cad_file']?>" alt="">
					</div>
					<?php } ?>
                </div>
            </div>
        </section>
        <section class="section-container" id="code">
			<a name="#code"></a>
			<span class="section-title two-title">项目代码</span><hr>
			<div class="section-content">
				<div class="repository">
					<div class="button-content-container">
						<div class="button-content one-font">
							<?php
							  if(isset($pro_meta['code_file'])){
							?>
								项目关键代码
							<?php }else{ ?>
								这个团队没有分享他们的项目代码
							<?php } ?>
							<div class="buttons" style="display:none">
								<a class="btn btn-primary btn-sm" href="<?php bloginfo('url');echo '/wp-content/'.$v['path']?>">下载 </a>
							</div>
						</div>
					</div>
					<?php
					  if(isset($pro_meta['code_file'])){
					?>
					<div class="embed original">
						<img src="<?php echo home_url().'/wp-content/'.$pro_meta['code_file']?>" alt="">
					</div>
					<?php } ?>
				</div>
            </div>
        </section>
<?php } 
	function show_detail_slider_project(){
?>		
        <li class="">
            <a class="smooth-scroll" href="#schematics">项目原理图</a>
        </li>
        <li>
            <a class="smooth-scroll" href="#cad">项目CAD图</a>
        </li>
        <li>
            <a class="smooth-scroll" href="#code">项目关键代码</a>
        </li>	
<?php  }?>
