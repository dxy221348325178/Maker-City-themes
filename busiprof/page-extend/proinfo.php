<?php
	global $wpdb;//定义全局变量
	global $current_user;
	wp_get_current_user();
?>
	<section id="project_info_bc" class="section-teaser">
		<div class="container-desktop container">
			<div id="home">
				<div class="project-title">
						<center><br/>
							<div class="one-title"><?php echo $pro['post_title'];?></div>
						</center>
					<?php 
					if($pro['post_author']){
						$auther = get_user_meta($pro['post_author'],'nickname',true);
					?>
					<div>
						<center><br/>
							<div class="two-title">发布者<a href="<?php echo home_url().'/personal-outline/' ?>"><?php echo $auther?></a></div>
						</center><br/>
					</div>
					<?php } ?>
				</div>
				<div class="row project-teaser">
					<div class="col-md-7 col-md-offset-0-5 left-column">
						<div class="project-cover-image">
							<img alt="项目展示图片" src="<?php echo home_url().'/wp-content/'.$pro_meta['pro_pic'] ?>" title='项目图片'>
						</div>
					</div>
					<div class="col-md-4 right-column">
						<div class="container-mobile">
							<section class="section-thumbs">
								<div class="two-title">项目简述</div>
								<!-- 项目摘要 -->
								<p>
									<?php if($pro['post_excerpt']) echo $pro['post_excerpt']; ?>
								</p>
							</section>
							<section class="section-thumbs">
								<div class="two-title">项目信息</div>
								<div class="info-table three-title">
									<div class="info-row">
										<div>难度</div>
										<div class="info">
											<a class="project-difficulty text-warning" href="#"><?php echo ''; ?></a>
										</div>
									</div>
									<div class="info-row">
										<div class="info-label">项目持续时间</div>
										<div class="info"><?php if($pro_meta['duration']) echo $pro_meta['duration'].'　小时'; ?></div>
									</div>
									<div class="info-row">
										<div class="info-label">发布时间</div>
										<div class="info">
											<?php if($pro['post_date']) echo $pro['post_date']; ?>
										</div>
									</div>
									<div class="info-row">
										<div class="info-label">最新修改时间</div>
										<div class="info">
											<?php if($pro['post_modified']) echo $pro['post_modified']; ?>
										</div>
									</div>
								</div>
							</section>
							<section class="section-thumbs" id="respects-section">
								<ul class="list-inline text-muted small project-stats-inline two-title">
									<li style="display: none" class="impression-stats istooltip">
										<button>
											<i class="fa fa-thumbs-o-up">
												<span class="stat-figure" id = "like_btn" ><?php ;?></span>
											</i>
										</button>
									</li>
									<li style="display:none">
										<link href="#"
										<i class="fa fa-eye">浏览次数</i>
										<span class="stat-figure"><?php ; ?></span>
									</li>
									<li class="respect-stats istooltip">
										<link href="#">
										<i class="fa ">组成员</i>
										<span class="stat-figure"><?php echo count($pro_meta['pro_users']).'人'; ?></span>
									</li>
								</ul>
								<div class="respecting-faces" onclick="">
									<?php 
										foreach($pro_meta['pro_users'] as $value){
											$team_user = explode('#-user--do-#',$value);
											$team_user_pic = get_user_meta($team_user[0],'profile_photo',true);
											if($team_user_pic == ''){
												$team_user_pic = home_url()."/wp-content/uploads/ultimatemember/default/default.png";
											}else{
												$team_user_pic = home_url()."/wp-content/uploads/ultimatemember/".$team_user[0].'/'.$team_user_pic;
											}
									 ?>
									<div id="head_image_size" class="user-img">
										<img alt="<?php echo $team_user[0] ?>" title="<?php echo '贡献 : '.$team_user[1] ?>" class="img-circle img-loader loaded" src="<?php echo $team_user_pic ?>">
										<noscript></noscript>
									</div>
									<?php } ?>
								</div>
							</section>
						</div>
					</div>
				</div>
			</div>
		</div>
		<br/>
	</section>
	<div id="bgcolor_about_project">
		<center><br/>
			<div class="two-title"><?php if($pro['post_title']) echo $pro['post_title']; ?> -- 团队成员</div>
			<?php 
				foreach($pro_meta['pro_users'] as $value){
					$team_user = explode('#-user--do-#',$value);
					$team_user_pic = get_user_meta($team_user[0],'profile_photo',true);//头像
					if($team_user_pic == ''){
						$team_user_pic = home_url()."/wp-content/uploads/ultimatemember/default/default.png";
					}else{
						$team_user_pic = home_url()."/wp-content/uploads/ultimatemember/".$team_user[0].'/'.$team_user_pic;
					}
					$team_user_name = get_user_meta($team_user[0],'nickname',true);				
					$team_user_description = get_user_meta($team_user[0],'description',true);
			?>
			<div class=" lazy_image__root__3J0Tw" style="height: 36px; width: 36px;">
				<img alt="" class="lazy_image__image__hVuzg lazy_image__fadeIn__1kICV" src="<?php echo $team_user_pic ?>" />
			</div>
			<div>
				<a class="list__title__3G2mG" href="#">
					<font style="vertical-align: inherit;">
						<?php echo $team_user_name;?>
					</font>
				</a>
				<div class="list__subtitle__1mtOK">
					<font style="vertical-align: inherit;">
						<?php echo $team_user_description ?>
					</font>
				</div>
			</div>
		<?php } ?>
		</center>
		<section class="section-description">
			<div class="container">
				<div class="row">
					<div class="col-md-7 col-md-offset-0-5 left-column container-mobile">
					<?php 
					//正则进行字符串截取,将视频的关键词取出.
					$pattern = "/id_(.*?).html/U";//拼接正则
					preg_match($pattern, $pro_meta['show_url'], $str);
					$url = "http://player.youku.com/embed/".$str[1];//拼接视频窗口地址
					?>	
					<section class="section-container collapsed" id="about-project">
							<span class="section-title two-title">项目演示</span><hr>
						<?php if($str){ ?>
						<div class="embed-frame">
							<div class="figure youtube">
								<div class="figcaption embed-figcaption"></div>
								<div class="embed widescreen" contenteditable="false">
									<iframe width="100%" height="100%" src="<?php echo $url; ?>" frameborder="0" allowfullscreen=""></iframe>
								</div>
							</div>
						</div>
						<?php }else{ ?>
							<div class="section-content one-font">
								该团队没有上传他们的演示视频	
							</div>
						<?php } ?>
					</section>
					<section class="section-container collapsed" id="info">
						<span class="section-title two-title">项目历程</span><hr>
						<div class="section-content">
							<!-- 项目详情 -->
							<?php echo $pro['post_content'] ?>
						</div>
					</section>
					<?php
						//显示CAD图、原理图、关键代码，只有管理员和当前项目作者才能看见这些内容
						//if(1){
							//show_detail_project();
						//}
					?>
					<section class="section-container" id="comments">
						<span class="section-title two-title">评论</span><hr>
					</section>
				</div>
				<!-- 侧边动态锚点 -->
				    <script>
						$(function(){
							//获取要定位元素距离浏览器顶部的距离
							var navH = $("#ce").offset().top;
							//滚动条事件
							$(window).scroll(function(){
							//获取滚动条的滑动距离
								var scroH = $(this).scrollTop();
								//滚动条的滑动距离大于等于定位元素距离浏览器顶部的距离，就固定
								if(scroH>=navH){
									$("#ce").css({position: 'fixed',top:50,right:100});
								}    
								else if(scroH<navH){
								$("#ce").css({position: "static"});            }            
							})              
						})
					</script>  
				<div id="ce" class="col-md-4 right-column">
					<section class="section-thumbs hidden-xs hidden-sm">
						<div class="affixable" id="project-side-nav">
							<div class="section-container" id="scroll-nav">
								<ul id="floating_frame" class="nav">
									<li class="">
										<a class="smooth-scroll" href="#home">
											<?php if($pro['post_title']) echo $pro['post_title'] ?>
										</a>
									</li>
									<li class="">
										<a class="smooth-scroll" href="#about-project">项目演示</a>
									</li>
									<li class="">
										<a class="smooth-scroll" href="#info">项目历程</a>
									</li>
									<?php
										//if(1){
											//show_detail_slider_project();
										//｝
									?>
									<li>
										<a class="smooth-scroll" href="#comments">评论<span class="nav-count"><?php?></span></a>
									</li>
								</ul>
							</div>
						</div>
					</section>
				</div>
			</div>
		</div>
	</div>	
	</section>