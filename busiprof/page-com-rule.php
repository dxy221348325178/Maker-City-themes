<?php 
/*  
* Template Name: 赛区下规则模版
*/
get_header();
?>
<?php
  // 输出内容
  echo do_shortcode('[show-contests-head]');
?>
<section class="main-section">
  	<div class="container">
  		<div class="container">
   			<div class="row equal-columns">
		   		<?php
		          // 输出内容
		          the_post(); the_content();
		        ?>
   			</div>
  		</div>
  	</div>
</section>
<?php get_footer();?>