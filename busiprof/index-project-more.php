<?php
/**
 * Template Name: 全部项目
 */
get_header();
?>
<div id="main">
   <div data-react-class="Projects"  data-hydrate="t">
      <div class="layout__container__3L3Cc" data-reactroot="">
         <div class="layout__wrapper1170__3EDsw layout__wrapper__1EOxq">
            <h1 class="typography__reset__3SqaR typography__headerL__3T4vn projects_page__header__2k4oW">
               <font style="vertical-align: inherit;">
                  <font style="vertical-align: inherit;">特色项目</font>
               </font>
            </h1>
            <div>
               <div class="grid__gridBasic__fjt5B grid__grid__1QeD6 grid__guttersH__2MYvz grid__guttersV__3M28R " > 
               <?php
                  // 输出项目内容
                  the_post(); the_content();
               ?>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>   
<?php get_footer(); ?> 

