<!-- 平台公共导航 -->
<?php 
/*  
* Template Name: 平台公共导航
*/
get_header();

?>
<div>
   <div class="layout__headerContainer__3tDkO layout__container__3L3Cc header__root__1F7eL" id="reactSceneHeader">
      <div class="layout__wrapperCenter1170__3xUpU layout__wrapperCenter__6qTjc">
         <!-- 平台公共导航部分 -->
         <?php 
            // do_shortcode('[show-platform]');
            show_platform();
         ?>
         <div id="main">
            <div data-react-class="Projects"  data-hydrate="t">
               <div class="layout__container__3L3Cc" data-reactroot="">
                  <div class="layout__wrapper1170__3EDsw layout__wrapper__1EOxq">
                     <hr>
                     <div>
                        <div class="grid__gridBasic__fjt5B grid__grid__1QeD6 grid__guttersH__2MYvz grid__guttersV__3M28R " > 
                        <?php
                           // 输出项目内容
                           // the_post(); the_content();
                           echo do_shortcode('[show-platform-project step=8]');
                        ?>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php get_footer();?>